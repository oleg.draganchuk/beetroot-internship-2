<nav class="top_menu">
    <ul class="inside">
        <?php
        $args = array(
            'container'       => '',
            'items_wrap'      => '%3$s',
            'theme_location'  => 'footer_menu'
        );
        ?>
        <?php wp_nav_menu($args); ?>
    </ul>   
</nav>

    <?php wp_footer(); ?>
</body>
</html>