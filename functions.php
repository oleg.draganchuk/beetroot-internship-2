<?php

// register scripts and styles
add_action( 'wp_enqueue_scripts', 'my_scripts_and_styles');

//register menus
add_action( 'after_setup_theme', 'register_menus');

//register widget zones
add_action( 'widgets_init', 'widgets_zones' );

function my_scripts_and_styles() {
    wp_enqueue_script(
        'my-scripts',
        get_template_directory_uri().'/js/scripts.js', 
        array('jquery'),
        '1.0.1',
        true
    );

    wp_enqueue_style(
        'my-styles', 
        get_template_directory_uri().'/css/style.css', 
        array(),
        '0.1.0',
        'all'
    );
}

function register_menus() {
    register_nav_menus( [
		'header_menu' => 'Header menu',
		'footer_menu' => 'Footer menu'
	]);
}

function widgets_zones() {
    register_sidebar( ['name'=> 'Sidebar 1', 'id' => 'sidebar-1'] );
    register_sidebar( ['name'=> 'Sidebar 2', 'id' => 'sidebar-2'] );
    register_sidebar( ['name'=> 'Sidebar 3', 'id' => 'sidebar-3'] );
}



