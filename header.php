<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>The HTML5 Herald</title>
    <meta name="description" content="simple starter">
    <?php wp_head(); ?>
</head>
<body>
    <nav class="top_menu">
        <ul class="inside">
            <?php
            $args = array(
                'container'       => '',
                'items_wrap'      => '%3$s',
                'theme_location'  => 'header_menu'
            );
            ?>
            <?php wp_nav_menu($args); ?>
        </ul>   
    </nav>