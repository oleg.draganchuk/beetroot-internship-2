
<?php get_header(); ?>

    <div class="wrapper">
    <?php if (have_posts()) : while ( have_posts() ) : the_post();?>
            <article>
            <h1>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h1>
            <?php the_excerpt(); ?>
            </article>
        <? endwhile; endif;  ?>
    </div>

    <aside>

        <ul>
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </ul>

        <ul>
            <?php dynamic_sidebar( 'sidebar-2' ); ?>
        </ul>

        <ul>
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </ul>

    </aside>

<?php get_footer(); ?>

